from django.core.management.base import BaseCommand
from app.internal.bot import run


class Command(BaseCommand):
    help = 'Запускает телеграм бота'

    def handle(self, *args, **options):
        run()
