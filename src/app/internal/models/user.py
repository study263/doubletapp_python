from django.core.validators import RegexValidator, MaxValueValidator, MinValueValidator
from django.db import models


class User(models.Model):
    telegram_user_id = models.IntegerField(
        validators=[MinValueValidator(-9223372036854775808), MaxValueValidator(9223372036854775807)],
        null=True)
    phone_regex = RegexValidator(
        regex=r'^\d{10}$',
        message="Телефон должен быть указан в формате 10 цифр без кода страны (+7 или 8). Пример: '9998887766'")
    phone = models.CharField(validators=[phone_regex], max_length=10, null=True)

    class Meta:
        db_table = 'users'

    def __str__(self):
        return f'Телеграм идентификатор пользователя: {self.telegram_user_id}\n' \
               f'Номер телефона: {self.phone}'


