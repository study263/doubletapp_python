from django.core.exceptions import ValidationError
from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services import user_service
from app.internal.services.validators.user_validators import phone_required


def start(update: Update, _: CallbackContext):
    user_service.create_or_update_user(update.effective_user.id)
    update.message.reply_text('Пока бот умеет только сохранять о вас информацию')


def set_phone(update: Update, context: CallbackContext):
    if not context.args:
        update.message.reply_text(
            f"Номер телефона обязателен. \nПример использования: '/{set_phone.__name__} +79998887766'")

        return

    phone = context.args[0]

    try:
        user_service.create_or_update_user(update.effective_user.id, phone)
    except ValueError as e:
        update.message.reply_text(str(e))

        return
    else:
        update.message.reply_text('Номер телефона обновлён')


def me(update: Update, _: CallbackContext):
    try:
        phone_required(telegram_user_id=update.effective_user.id)
    except ValidationError as e:
        update.message.reply_text(str(e))

        return

    user = user_service.get(update.effective_user.id)

    update.message.reply_text(str(user))
