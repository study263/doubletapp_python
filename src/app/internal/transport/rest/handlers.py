from django.http import HttpResponse
from django.core.exceptions import ValidationError
from app.internal.services import user_service
from app.internal.models.user import User
from app.internal.services.validators.user_validators import phone_required


def me(request):
    telegram_user_id = request.GET.get('telegram_user_id', None)

    try:
        phone_required(telegram_user_id=telegram_user_id)
    except ValidationError as e:
        return HttpResponse(str(e))

    if not telegram_user_id:
        return HttpResponse(
            "Не передан телеграм идентификатор пользователя. Пример использования: '.../me?telegram_user_id=23829283'")

    try:
        user = user_service.get(telegram_user_id)
    except User.DoesNotExist:
        return HttpResponse(f"Пользователь с идентификатором '{telegram_user_id}' не найден")

    return HttpResponse(str(user))
