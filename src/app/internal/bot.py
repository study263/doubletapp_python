from telegram.ext import Updater, CommandHandler
from config.settings import TELEGRAM_TOKEN

from app.internal.transport.bot.handlers import start, set_phone, me


def add_handlers(updater: Updater):
    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CommandHandler('set_phone', set_phone))
    updater.dispatcher.add_handler(CommandHandler('me', me))


def run():
    updater = Updater(TELEGRAM_TOKEN)

    add_handlers(updater)

    updater.start_polling()
    updater.idle()
