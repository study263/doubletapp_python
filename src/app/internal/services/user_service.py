import re

from app.internal.models.user import User


def create_or_update_user(telegram_user_id: int, phone: str = None) -> bool:
    validated_phone = validate_and_get_phone(phone)

    _, created = User.objects.update_or_create(
        telegram_user_id=telegram_user_id, defaults={'telegram_user_id': telegram_user_id, 'phone': validated_phone})

    return created


def validate_and_get_phone(phone: str = None):
    if not phone:
        return None

    phone_match = re.fullmatch(r'^(?:\+7|8)?(\d{10})$', phone)
    if not phone_match:
        raise ValueError('Номер телефона должен быть в формате: +79998887766 или 89998887766 или 9998887766')

    return phone_match.group(1)


def get(telegram_user_id: int) -> User:
    return User.objects.get(telegram_user_id=telegram_user_id)
