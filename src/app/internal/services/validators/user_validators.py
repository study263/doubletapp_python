from django.core.exceptions import ValidationError

from app.internal.models.user import User


def phone_required(**kwargs):
    has_phone = User.objects.filter(**kwargs).exclude(phone=None).exists()

    if not has_phone:
        raise ValidationError('У пользователя должен быть номер телефона')
